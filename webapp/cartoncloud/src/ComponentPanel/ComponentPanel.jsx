/* eslint-disable object-curly-newline */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Groups from '../Groups';

class ComponentPanel extends Component {

  /* eslint react/forbid-prop-types: 0 */
  static propTypes = {
    productList: PropTypes.array,
    groupList: PropTypes.array,
  };

  static defaultProps = {
    productList: [],
    groupList: [],
  };

  constructor(props) {
    super(props);

  }

  state = {
    groups: [<Groups />],
  }

  addGroup = () => {
    this.setState({
      groups: [...this.state.groups, <Groups />],
    })
  }

  

  render() {
    return (
      <div>
          {this.state.groups}
          <div>
            <button onClick={this.addGroup}>Add Group</button>
          </div>
      </div>
    );
  }
}

export default ComponentPanel;
