/* eslint-disable object-curly-newline */
import React, { Component } from 'react';

class Products extends Component {

  constructor(props){
    super(props);
    this.state = {
      rows: [],
    };
  }

  deleteRow = () => {
    this.setState({
      rows: this.state.rows.filter(e => !e.checked),
    });
  }

  render() {
    return (
      <div style={{width: "30%", display: "table"}}>
        <div>
          <label>Product</label>
        </div>

        <div style={{display: "table-row"}}>
          <div style={{display: "table-cell"}}>
            <select>
              <option value="product1">Product 1</option>
              <option value="product2">Product 2</option>
              <option value="product3">Product 3</option>
            </select>
          </div>

          <div style={{display: "table-cell"}}>
            X
          </div>

          <div style={{display: "table-cell"}}>
            <input  
            type="range" 
            min="0" max="10" 
            step="1"/>
          </div>

          <div style={{display: "table-cell"}}>
            <button onClick={this.deleteRow}>X</button>
          </div>

        </div>
      </div>
    );
  }
}

export default Products;
