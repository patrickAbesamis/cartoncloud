import React from 'react';
import logo from './logo.svg';
import './App.css';
import ComponentPanel from './ComponentPanel';

function App() {
  return (
    <div className="App" style={{display: 'flex', justifyContent: 'center'}}>
      <div>
        <label>Name: </label>
        <input
          type="text"
        />
        <ComponentPanel/>
      </div>
      <div>
        <button>Save</button>
        <button>Cancel</button>
      </div>
    </div>
  );
}

export default App;
