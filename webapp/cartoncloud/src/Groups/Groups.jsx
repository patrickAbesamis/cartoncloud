/* eslint-disable object-curly-newline */
import React, { Component } from 'react';

import Products from '../Products';

class Groups extends Component {

  state = {
    products: [<Products />],
  }

  addProduct = () => {
    this.setState({
      products: [...this.state.products, <Products />],
    })
  }

  

  render() {
    return (
      <div style={{width: "30%", display: "table"}}>
          <label>Group Label</label>

          <div style={{display: "table-row"}}>

            <div style={{display: "table-cell"}}>
              {this.state.products}
            </div>

          </div>

          <div style={{display: "table-row"}}>
            <button onClick={this.addProduct}>Add Product</button>
          </div>

      </div>
    );
  }
}

export default Groups;
